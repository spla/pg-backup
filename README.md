# pg_backup
Bash script to daily backup to a FTP server a Postgresql database.
This bash script will keep last three backups only at your FTP server and local dir.


### Dependencies

-   **remote FTP server account**

### Usage:

1. Edit config.txt and set your variables.

2. Run 'sh pg_dbbackup'.

3. Set up your favourite scheduling method to run pg_dbbackup regularly.

